#!/bin/bash
cd  /flac

for f in *.flac
do 
	echo "Processing file $f"
	ffmpeg -i "$f" -acodec alac "/alac/${f%.flac}.m4a"
done