# FLAC-2-ALAC Converter #

### Usage ###

docker run -it --rm -v path_to_dir_with_flac_files:/flac -v output_dir_path:/alac forinil/flac2alacconverter

See also [docker-compose.yml](docker-compose.yml)